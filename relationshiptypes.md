# Relationship Types

## Source 1

https://sugardaddie.com/blog/what-is-a-sugar-daddy-arrangement/

The type of Sugar Baby Arrangements depends on the following factors:

- The length of the relationship
- The duration of dates
- The frequency of dates
- The goals and needs of both parties

### Gift Me

This one is quite popular among the newcomers who are just entering the scene and are not 100% sure they are up for it. Gift Me arrangements are entry level, basic and typically uncomplicated agreements that are a good choice for sporadic dating and folks looking to add spice to their daily lives.

### Card Me

Card Me arrangement is similar to Gift Me arrangement, only here the Sugar Baby receives a gift card to an agreed value, which she can later use to pick out her own gifts. Neat.

### Home Me

This one is the big leagues! In a Home Me arrangement, a wealthy Sugar Daddy buys his Sugar Baby a house or an apartment and transfers the deeds into her name.

### Cash Out

Within the Cash Out arrangement, Sugar Daddies looking for Sugar Babies simply pay their ‘customers’ in cash in order to avoid a paper trail. This one is discreet, safe and solves any problems around the sharing of personal details. 

### Educate Me

This agreement typically involves the Sugar Daddy making regular monthly payments aimed at supporting the Sugar Baby’s education. Monthly meet-ups are the best fit for this setup.

