# Full of useful functions that I keep finding myself rewriting...

import numpy as np
import pandas as pd

# Given a Pandas dataframe and two columns, constructs a new dataframe containing the counts
# for each pair of values in the columns. Assumes columns are categorical, or at least integral,
# because that'd be really funny for floats.
# Output is suitable for shoving into a Seaborn heatmap.
def cross_frequencies(df, col1, col2, dropna=False):
    if dropna:
        ddf = df.dropna(subset=[col1, col2])
    else:
        ddf = df.fillna('N/A')
    col1_vals = ddf[col1].unique()
    col2_vals = ddf[col2].unique()
    
    out = pd.DataFrame(np.zeros((len(col1_vals), len(col2_vals))),
            index=col1_vals, columns=col2_vals)
    
    for index in col1_vals:
        for column in col2_vals:
            out[column][index] = len(ddf[(ddf[col2] == column) & (ddf[col1] == index)])
    
    return out
    


