Let's talk to myself a bit.

First step of the RM project is data exploration. The idea is to sift around and check for correlations. This should lead into some kind of clustering; customer segmentation. By breaking the customer base into groups, and finding which groups respond positively to each other, RM hopes to increase the number of conversations that get started.

Measuring that is *their* problem. Ours is just doing the data science.

We have a shitton of data. I list only the ones that might be interesting:

- Account Type (Generous or Attractive)
- Age
- Background Check Date (specifically, whether or not it's been done)
- Birthday
- Body Type (Average, Athletic, Curvy...)
- Children (number thereof)
- City
- Country
- Drinking Habit (Non, Social, Heavy)
- Ethnicity (be careful using this one...)
- Favorites Received
- Favorites Sent
- Gender (A minority of Generous Females and Attractive Males exists, but they *do* exist)
- Hair Color
- Income
- Language
- Marital Status
- Messages Received
- Messages Sent
- Net Worth
- Orientation
- Past Session Count (???)
- Private Photo Count (???)
- Profile Approval Status (???)
- Profile Completion Percent
- Public Photo Count
- Region (State/Province, seems)
- Smoking Habit (Non, light, heavy (no social?))
- Username

Let's ask a few questions and see what we shake out.

## Initial interesting observations

- Sugar babies almost universally don't post income. The majority of sugar daddies do. This is unsurprising.

